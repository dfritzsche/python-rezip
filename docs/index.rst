=============================
 Rezip - Repack ZIP Archives
=============================

.. include:: intro.rst

.. toctree::

   usage
   api
   changelog/CHANGELOG
   license

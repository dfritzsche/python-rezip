Repack ZIP archives with the possibility to update metadata, e.g.,
Unix access right bits. Rezip can be used to fix Windows generated ZIP
archives (e.g., Python wheel packages) so that the archives members
have suitable access rights for usage on Linux.

GitLab page: `https://gitlab.com/dfritzsche/python-rezip
<https://gitlab.com/dfritzsche/python-rezip>`_

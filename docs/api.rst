API of the :mod:`rezip` Module
==============================

.. autoclass:: rezip.Arguments

.. autofunction:: rezip.rezip

.. autofunction:: rezip.parse_args

Changelog history
=================


v0.5.1 (2019-06-21)
-------------------

* Use `sphinx-rst-builder
  <https://github.com/davidfritzsche/sphinx-rst-builder>`_ to create
  README.rst and CHANGELOG.rst. This way the full Sphinx markup can be
  used in the source files under ``docs/`` while creating rst files
  that can be previewd by online services like GitLab, Github and
  PyPI.


v0.5.0 (2019-05-26)
-------------------

* Set :attr:`~zipfile.ZipInfo.create_system` (PKZIP 4.4.2 *version
  made by*) to `3` (Unix) when modifying
  :attr:`~zipfile.ZipInfo.external_attr` to set Unix permissions.
  Unix permissions stored in :attr:`~zipfile.ZipInfo.external_attr`
  are not correctly restored by :mod:`zipfile` or Info-ZIP if
  :attr:`~zipfile.ZipInfo.create_system` is `0` (FAT).


v0.4.1 (2019-05-21)
-------------------

* Updated README.


v0.4.0 (2019-05-21)
-------------------

* New ``--glob`` (or ``-g``) option to treat input file names as shell
  glob patterns.
* Allow multiple input files when using ``--inplace``.
* New ``--output`` (or ``-o``) option to specify the output file.


v0.3.2 (2019-05-19)
-------------------

* Documentation improvements


v0.3.0 (2019-05-19)
-------------------

* New ``--date-time`` option to set file date time in the ZIP file


v0.2.2 (2019-05-14)
-------------------

* Include LICENSE file in sdist


v0.2.1 (2019-05-13)
-------------------

* Packaging improvements


v0.2.0 (2019-05-13)
-------------------

* Updated documentation


v0.1.0 (2019-05-13)
-------------------

* Initial public release

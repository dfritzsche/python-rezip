# -*- coding: utf-8 -*-
# SPDX-License-Identifier: MIT

import os, sys
self_dir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(os.path.join(self_dir, "..", ".."))

from docs.conf import *

master_doc = os.path.basename(self_dir).upper()

Sources for the tiny executables
================================

tiny45.asm - https://www.muppetlabs.com/~breadbox/software/tiny/teensy.html
tinyelf32.asm - https://www.muppetlabs.com/~breadbox/software/tiny/revisit.html
tinyelf64.asm - https://blog.stalkr.net/2014/10/tiny-elf-3264-with-nasm.html


Alternatives
============

Small ELF64 program:

;; https://stackoverflow.com/a/53383541
bits 64
global _start
_start:
   mov di,42        ; only the low byte of the exit code is kept,
                    ; so we can use di instead of the full edi/rdi
   xor eax,eax
   mov al,60        ; shorter than mov eax,60
   syscall          ; perform the syscall

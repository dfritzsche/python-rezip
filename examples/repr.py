#!/usr/bin/env python3
if __name__ == "__main__":
    import sys
    for fname in sys.argv[1:]:
        with open(fname, "rb") as f:
            print(repr(f.read()))

# pylint: disable=missing-docstring
import pytest
from rezip import parse_args


@pytest.mark.parametrize(
    "argv",
    [
        ["dummy", "--inplace"],
        ["dummy", "--inplace", "in"],
        ["dummy", "--inplace", "in", "in2"],
        ["dummy", "-o", "out", "in"],
    ],
)
def test_parse_valid_args(argv):
    parse_args(argv=argv)


def test_parse_args_output_required_if_not_inplace():
    argv = ["dummy", "in"]
    with pytest.raises(SystemExit):
        parse_args(argv=argv)


def test_parse_args_multiple_input_one_output_not_allowed():
    argv = ["dummy", "-o", "out", "in", "in2"]
    with pytest.raises(SystemExit):
        parse_args(argv=argv)


def test_parse_args_input_required_when_output_is_present():
    argv = ["dummy", "-o", "out"]
    with pytest.raises(SystemExit):
        parse_args(argv=argv)


def test_parse_args_glob(monkeypatch):
    import glob

    zip_files = ["a.zip", "x.zip", "y.zip"]

    def my_iglob(pathname, *, recursive=False):  # pylint: disable=unused-argument
        yield from zip_files

    def my_glob(pathname, *, recursive=False):  # pylint: disable=unused-argument
        return zip_files[:]

    monkeypatch.setattr(glob, "iglob", my_iglob)
    monkeypatch.setattr(glob, "glob", my_glob)

    # Too many input files / no output file
    with pytest.raises(SystemExit):
        parse_args(["dummy", "-g", "*.zip"])

    # Check that "*.zip" expands to zip_files
    args = parse_args(["dummy", "--inplace", "-g", "*.zip"])
    assert set(args.inputs) == set(zip_files)

    zip_files[:] = []
    args = parse_args(["dummy", "--inplace", "-g", "*.zip"])
    assert set(args.inputs) == set()


def test_parse_args_verbosity():
    assert parse_args(["", "-q"]).verbosity < 0
    assert parse_args([""]).verbosity == 0
    assert parse_args(["", "-v"]).verbosity == 1
    assert parse_args(["", "-vv"]).verbosity == 2

import datetime

from hypothesis import given, strategies as st
from rezip import to_isoformat


@given(st.integers(min_value=0, max_value=2147483647))
def test_timestamp_to_isoformat(i):
    assert to_isoformat(i) == datetime.datetime.fromtimestamp(i).isoformat(sep=" ")


@given(
    dt=st.datetimes(
        min_value=datetime.datetime(1970, 1, 1),
        max_value=datetime.datetime(2038, 1, 19, 3, 14, 7),
    )
)
def test_datetime_to_isoformat(dt):
    assert to_isoformat(dt) == dt.isoformat(sep=" ")

    # For the tuple roundtrip, we cannot work with microseconds precision
    dt = dt.replace(microsecond=0)
    assert to_isoformat(dt.timetuple()[:6]) == dt.isoformat(sep=" ")

#!/usr/bin/env python3
"""Run setup"""
import sys

import setuptools


def main():
    """Run setup"""
    setuptools.setup()


if __name__ == "__main__":
    sys.exit(main())
